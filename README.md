# sot-edrans-app

## Name
SOT-Edrans App.



## Description

This repository is part of the Docker course for Edrans SoT.

In the course will approach Docker from a practical concept and aiming to use docker professionally in a working environment.

This is an example project to work with docker based on code  https://github.com/pablokbs/peladonerd/tree/master/docker/12 published by @pablokbs on https://www.youtube.com/watch?v=CV_Uf3Dq-EU

This course goes through the following topics:

1. Intro Docker: Old school vs New school.
2. Diferences between Docker and Virtutal machines (VMs).
3. Docker's components.
4. Install Docker.
5. Docker images and docker run.
6. Common commands.
7. Dockerfile.
8. Docker build, pull, push, save.
9. Docker port and networking.
10. Docker volumes.
11. Docker-Compose.
12. Intro CI/CD.
13. Continuos Integration.
14. Continuos Delivery.
15. GitOps: Pipelines and jobs.
16. GitLab Runners: Shared runners and Specific runners.
17. Makefile: Developer env.
18. Gitlab Registry.
19. gitlab-ci: Staging env deployment, secrets variables.
20. Using nginx proxy and let's encrypt container.

## Roadmap

To run this proyect you need:

1. Install docker, docker-compose in VM target.
   Script: https://gitlab.com/nicrom/sot-edrans/-/blob/main/init-docker.sh
2. Install and register gitlab runner in VM target.
3. Create CI/CD variables.
4. Deploy nginx proxy and acme companion. https://gitlab.com/nicrom/sot-edrans/-/tree/main/proxy
   Note: create network `docker network create https-proxy`
5. Regiter domain record pointng to VM target, and update HOST vars in https://gitlab.com/nicrom/sot-edrans-app/-/blob/main/deployer/docker-compose.yml
6. Create backend network in VM target: `docker network create backend-app`
7. Update TAG variable in `.gitlab-ci` to point to correct registry.
7. Run pipeline from `staging` branch.
